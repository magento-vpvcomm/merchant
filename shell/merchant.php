<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 10.01.17
 * Time: 8:28
 */
session_start();
date_default_timezone_set("Europe/Moscow");

require_once 'global/cli_config.php';
require_once 'abstract.php';
ini_set('memory_limit', '6048M');

use Console as C;

class Mage_Shell_Merchant extends JobSelector {

    protected $storeId = false;
    protected $storeName = false;
    protected $storeReal = false;
    protected $storeChain = false;

    // csv prepare
    private $csvArray = [];
    private $csvColumns = ['marker','vpvcomm_id','vpvcomm_chain','google_id','google_chain'];

    private $tableTaxonomy = 'agm_merchant_taxonomy';
    private $tableMapping = 'agm_mapping_store';
    private $tableSynonym = 'agm_synonym';

    // сюда вписываем идентифицированные категории
    private $readyCategoriesList = [];
    

    /**
     * поиск аналогов наших категорий в Google-категориях
     * @return string
     */
    public function generate()
    {
        /**
         * ПОДГОТОВКА к работе с определенным стором
         */
        $arr_store = [];
        $pop = Mage::app()->getStores();

        foreach ($pop  as $val) {
            $arr_store[$val->getId()] = $val->getName();
        }

        $store = selector('<red>Выберите стор</red>', $arr_store, true, null, '0');

        $this->storeId = $store;
        $this->storeName = $arr_store[$store];
        $this->storeReal = Mage::app()->getStore($this->storeId)->getRootCategoryId();

        $notice = "\r\n<grey>Выбран магазин</grey> <yellow>{$this->storeName}</yellow> <grey>за номером</grey> <purple>{$this->storeId}</purple>\r\n";

        out($notice);

        /**
         * ОБРАБОТКА категорий определенного стора
         */

        $resource   = Mage::getSingleton('core/resource');
        $connection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection('core_write');

        $categories = Mage::getModel('catalog/category')
            ->getCollection()
            ->addAttributeToSelect('*');

        $arrSyn = $this->getSynonymFromTable();

        // csv prepare
        $arrReady[] = ['ГОТОВО'];
        $arrReady[] = $this->csvColumns;
        $arrSomeVars[] = ['ВЫБРАТЬ ИЗ ВАРИАНТОВ'];
        $arrSomeVars[] = $this->csvColumns;
        $arrRequired[] = ['НАЗНАЧИТЬ ВРУЧНУЮ'];
        $arrRequired[] = $this->csvColumns;
        $countReady = 0;
        $countSomeVars = 0;
        $countRequired = 0;

        foreach ($categories as $item) {
            $catName = trim($item->getName());
            $parents_arr = $item->getParentIds();

            // анализируем только конечные категории ---------------------------------
            if (!$item->getChildren() && in_array($this->storeReal,$parents_arr)) {

                $vpvcommId = $item->getId();

                $cat2 = $item->getParentCategory();
                $cat2name = $cat2->getName();
                $cat1 = $cat2->getParentCategory();
                $cat1name = $cat1->getName();

                $this->storeChain = "{$cat1name} > {$cat2name} > {$catName}";
                echo $this->storeChain."\r\n";

                $catName = $this->runSynonymTable($catName,$arrSyn);
                $like = $this->prepareLike($catName,true);

                // одно слово 'принадлежности' нельзя пускать в поиск
                // будет слишком много результатов
                if ($like == 'принадлежности') {
                    $cat2name = mb_strtolower($cat2name);
                    // избегаем склонения
                    $cat2name = mb_substr($cat2name,0,-2);
                    $addParent = $this->prepareLike($cat2name,true);
                    $like = $like.'%'.$addParent;
                }

                $query = "SELECT * FROM $this->tableTaxonomy WHERE `chain_gm` LIKE '%{$like}%'";

                $rows = $connection->fetchAll($query);

                $resCount = count($rows);

                if ($resCount == 1) {
                    $countReady++;
                    $arrReady[] = ['vpvcomm-ready',$vpvcommId,$this->storeChain,$rows[0]['id_gm'],$rows[0]['chain_gm']];
                    $googleId = $rows[0]['id_gm'];
                    $googleChain = $rows[0]['chain_gm'];
                    $query = "NULL, $this->storeReal, $vpvcommId, $googleId";
                    $this->insertIgnore($query,$writeConnection);
                    array_push($this->readyCategoriesList,[$this->storeId,$this->storeReal,$vpvcommId,$this->storeChain, 'соответствует Google Category ID:  '.$googleId]);

                    $item->setData('merchant_id', $googleId );
                    $item->save();

                    out("<green>Один результат = отправляем сразу в БД</green>");
                } elseif ($resCount > 1) {
                    $countSomeVars++;
                    out("<purple>Несколько результатов = предлагаем выбор в консоли</purple>");
                    $goodRows = $this->iterateRows($rows);
                    //$set = selector('<red>Выберите одно значение</red>', $goodRows, true, null, '0');
                    $arrSomeVars[] = ['vpvcomm-somevars',$vpvcommId,$this->storeChain,"",""];

                    foreach ($goodRows as $key => $val) {
                        $arrSomeVars[] = ['','',"\t\t{$key} => {$val}"];
                    }

                } else {
                    $countRequired++;
                    out("<red>Ничего нет = начинаем изгаляться</red>");
                    $arrRequired[] = ['vpvcomm-required',$vpvcommId,$this->storeChain,"",""];
                }

            }

        }

        ### --- CSV checklist create -------------------------------------------

        $arrReady[0][0] = $arrReady[0][0]." = {$countReady}";
        array_push($arrReady,[""],[""],[""]);

        $arrSomeVars[0][0] = $arrSomeVars[0][0]." = {$countSomeVars}";
        array_push($arrSomeVars,[""],[""],[""]);

        $arrRequired[0][0] = $arrRequired[0][0]." = {$countRequired}";

        $createAt[] = ["Скрипт выполнен: ". date("d.m.Y H:i:s")];
        array_push($createAt,[""],[""],[""]);

        $this->csvArray = array_merge($createAt,$arrReady,$arrSomeVars,$arrRequired);

        // чтобы менеджер не ошибся куда он что загружает
        // добавим к названию чеклиста название стора
        $storeNameNew = "{$this->storeName}-{$this->storeId}-{$this->storeReal}";
        $arr_file = $this->configCSV($storeNameNew);

        try {
            $fp = fopen($arr_file['full_path'], 'w');
            foreach ($this->csvArray as $fields) {
                fputcsv($fp, $fields, ';');
            }
            fclose($fp);
            $this->sendNotice($arr_file['full_path'],$this->readyCategoriesList);
        } catch (Exception $e) {
            Mage::printException($e);
            Mage::logException($e);
        }

        echo "\r\n\r\n";
        out("<green>Гоотовые соответствия: {$countReady}</green>");
        out("<yellow>Выбрать из нескольких: {$countSomeVars}</yellow>");
        out("<red>Назначить вручную: {$countRequired}</red>");
        echo "\r\n\r\n";
    }

    private $checklistPathDir = '/media/merchant/';
    private $checklistNameFile = 'merchant_checklist.csv';
    private $checklistFullPath = false;

    /**
     * парсинг чеклиста Google Merchant исправленного контент-менеджером
     */
    public function parse()
    {
        $this->checklistPathDir = getcwd().$this->checklistPathDir;
        $arg1 = ask('Название файла', null, true, 'arg1');
        $this->checklistFullPath = $this->checklistPathDir.$arg1;
        if (file_exists($this->checklistFullPath)) {
            $this->parseCSV($this->checklistFullPath);
        } else {
            out('<yellow>Нет такого файла</yellow>: <green>{$arg1}</green>');
        }
    }

    /**
     * обнуляем аттрибут merchant_id для стора
     */
    public function toEmpty()
    {
        $arr_store = [];
        $pop = Mage::app()->getStores();
        foreach ($pop  as $val) {
            $arr_store[$val->getId()] = $val->getName();
        }
        $storeId = selector('<red>Выберите стор</red>', $arr_store, true, null, '0');
        $rootCategoryId = Mage::app()->getStore($storeId)->getRootCategoryId();
        $storeName = Mage::app()->getStore($storeId)->getName();
        $notice = "\r\n<grey>Выбран магазин</grey> <yellow>{$storeName}</yellow> <grey>за номером</grey> <purple>{$storeId}</purple>\r\n";
        out($notice);
        $rootpath = Mage::getModel('catalog/category')
            ->setStoreId($storeId)
            ->load($rootCategoryId)
            ->getPath();
        $categories = Mage::getModel('catalog/category')->setStoreId($storeId)
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('path', array("like"=>$rootpath."/"."%"));
        $items = $categories->getItems();
        $arg1 = ask("Вы уверены, что хотите удалить все значения\r\nдля аттрибута 'merchant_id'?", 'no', true, 'arg1');

        if ($arg1 == 'no') {
            die(out("<red>Вы успешно отказались от удаления аттрибута 'merchant_id'</red>"));
        } elseif ($arg1 == 'yes') {

            foreach ($items as $item) {
                $merhId = $item->getData('merchant_id');
                if ($merhId) {
                    $item->setData('merchant_id', NULL);
                    $item->save();
                }
            }

        } else {
            out("<blue>Нет такого значения!</blue>");
        }
    }

    /**
     * парсинг ческлиста в формате csv
     * с выполнением определенной логики
     * @param $checklistFullPath
     */
    protected function parseCSV($checklistFullPath)
    {
        $row = 1;
        $resource   = Mage::getSingleton('core/resource');
        $connection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection('core_write');
        $chlist = $this->getFileInfo($checklistFullPath);
        $storeId = $chlist['store_id'];
        $storeRootId = $chlist['store_root_id'];
        $storeName = $chlist['store_name'];
        $storeNameLower = strtolower($storeName);
        $arrChange = [];

        /**
            [date] => 2017-01-16
            [time] => 12:42:22
            [file_name] => 19_1484570542_merchant_checklist.csv
            [store_id] => 19
            [store_root_id] => 496
            [store_name] => Evo Philips Store
         */

        // проверка чеклиста перед парсингом и заливкой в бд
        // если вдруг менеджер попытается залить чеклист с одного проекта на другой
        $queryCheck = "SELECT trim(lower(`name`)) as 'name', `root_category_id`, `default_store_id` FROM `core_store_group` 
WHERE `root_category_id` = {$storeRootId} AND `default_store_id` = {$storeId};";
        $existsCheck = $connection->fetchOne($queryCheck);
        if (!$existsCheck) {
            $err = "\tОшибка при проверке стора!\r\n\tВ таблице `core_store_group` нет такого сочетания `root_category_id`и `default_store_id`= {$storeRootId} + {$storeId}";
            die(out("<gold>{$err }</gold>"));
        }
        try {
            if (($handle = fopen($checklistFullPath, "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 2000, ";")) !== FALSE) {
                    $num = count($data);
                    if ($num == 5) {
                        $marker         = trim($data[0]);
                        $vpvcomm_id     = trim($data[1]);
                        $vpvcomm_chain  = trim($data[2]);
                        $google_id      = trim($data[3]);
                        $google_chain   = trim($data[4]);
                        $noWorkColumns = ['marker','vpvcomm-ready'];

                        // обработка только тех строк файла
                        // которые содержат полезную информацию
                        // для изменения
                        if (
                            !empty($marker) &&
                            !in_array($marker,$noWorkColumns) &&
                            !empty($google_id)
                        ) {

                            $str = "marker: $marker, vpvcomm_id: $vpvcomm_id, vpvcomm_chain: $vpvcomm_chain, google_id: $google_id, google_chain: $google_chain";
                            echo $str."\r\n";
                            $query = "NULL, $storeRootId, $vpvcomm_id,  $google_id ";
                            array_push($arrChange,[$chlist['store_id'],$chlist['store_root_id'],$vpvcomm_id,$vpvcomm_chain, 'соответствует Google Category ID:  '.$google_id]);
                            $this->insertIgnore($query,$writeConnection);

                            $category = Mage::getModel('catalog/category')->load($vpvcomm_id);
                            $category->setData('merchant_id', $google_id);
                            $category->save();

                            echo $str."\r\n";
                        }

                    }
                    $row++;
                }
                fclose($handle);
                echo "\r\n";

                $readyList = $this->createReadyList($arrChange);
                $fname = $chlist['file_name'];
                $mail = new Zend_Mail('UTF-8');
                $htmlBody = <<<HTML
На сервер загружен исправленный чеклист с названием {$fname}
{$readyList}
HTML;

                $mail->setBodyText($htmlBody);
                $mail->setFrom('robot-checklist@vpvcomm.pw', 'Checklist Uploaded');
                $mail->addTo('p.vasin@vpvcomm.pw', 'AGM Developer');
                $mail->setSubject('shell: Загружен исправленный чеклист '.$chlist['store_name']);
                try {
                    $mail->send();
                } catch (Exception $ex) {
                    Mage::printException($ex);
                    Mage::logException($ex);
                }
            }
        } catch (Exception $e) {
            Mage::printException($e);
            Mage::logException($e);
        }

    }

    /**
     * формирование единого конфига
     * для управления создаваемым чеклистом
     * @return array
     */
    protected function configCSV($storeName=false)
    {
        $prefix = time();
        if ($storeName) {
            $storeName = strtolower($storeName);
            $storeName = str_replace(' ','_',$storeName);
            $prefix = $storeName . '-' . $prefix . '-';
        }
        $admFileName = Mage::getStoreConfig('vpvcomm_googlemerchant/agm_checklist/checklist_filename');
        $fileName = empty($admFileName) ? $this->checklistNameFile : $admFileName;
        $dirPath = getcwd(). $this->checklistPathDir;
        $fileName = $prefix . $fileName;
        $arr = [];
        $arr['dir_path'] = $dirPath;
        $arr['file_name'] = $fileName;
        $arr['full_path'] = $dirPath . $fileName;
        return $arr;
    }

    /**
     * формирование данных описывающих файл
     * на основе его названия
     * @param $checklistFile
     * @return array
     */
    protected function getFileInfo($checklistFile)
    {
        $arrResult = [];
        if (strpos($checklistFile,'/') !== false) {
            $farr = explode('/',$checklistFile);
            $fileName = end($farr);
        } else {
            $fileName = $checklistFile;
        }

        // получить название файла начинающееся с первых цифр
        // если есть маркер указывающий на внесеные правки
        if (strpos($fileName,'rewr') !== false) {
            $fileNameClear = str_replace('rewr_','',$fileName);
        } else {
            $fileNameClear = $fileName;
        }

        /**
        [0] => evo_philips_store
        [1] => 19
        [2] => 496
        [3] => 1484910204
        [4] => merchant_checklist.csv
         */

        $arr = explode('-',$fileNameClear);
        $storeName = $arr[0];
            $storeName = str_replace('_',' ',$storeName);
        $storeId = $arr[1];
        $storeRootId = $arr[2];
        $fileDate = $arr[3];
        $date = date('Y-m-d',$fileDate);
        $time = date('H:i:s',$fileDate);
        $arrResult['date'] = $date;
        $arrResult['time'] = $time;
        $arrResult['file_name'] = $fileName;
        $arrResult['store_id'] = $storeId;
        $arrResult['store_root_id'] = $storeRootId;
        $arrResult['store_name'] = $storeName;
        return $arrResult;
    }

    /**
     * @param $readyCategoriesList
     * @return string
     */
    private function createReadyList($readyCategoriesList)
    {
        /**
        [0] => 19
        [1] => 496
        [2] => 484
        [3] => Для матери и ребенка > Наблюдение и безопасность > Видеоняни
         */
        $str = '';
        if ($readyCategoriesList) {
            foreach ($readyCategoriesList as $val) {
                $str .= "<li>{$val[0]} -> {$val[1]} -> {$val[2]} -> {$val[3]} = {$val[4]}</li>";
            }
            $str = '<ol>'.$str .'</ol>';
            $str = "<hr><b>Перечень идентифицированных категорий:</b>".$str;
        }
        return $str;
    }

    /**
     * отправка письма менеджеру
     * с вложенным csv-чеклистом
     */
    protected function sendNotice($checklistFile,$readyCategoriesList=false)
    {
        $dateTime = $this->getFileInfo($checklistFile);
        $mail = new Zend_Mail('UTF-8');
        $readyList = $this->createReadyList($readyCategoriesList);
        $bodyText = <<<HTML
Вы получили это письмо потому, что вы являетесь ответственным лицом: ваш email указан в настройках модуля.
Создан чеклист для стора {$dateTime['store_name']} за номером {$dateTime['store_id']}. Корневая категория № {$dateTime['store_root_id']}.
{$dateTime['date']} в {$dateTime['time']} был создан файл {$dateTime['file_name']}
Скачайте, проверьте его и произведите соответствующие замены.
{$readyList}
HTML;
        $admMailTo = Mage::getStoreConfig('vpvcomm_googlemerchant/agm_general/email_manager');
        $mailTo= empty($admMailTo) ? 'p.vasin@vpvcomm.pw' : $admMailTo;
        $mail->setBodyText($bodyText);
        $mail->setFrom('agm_robot@vpvcomm.pw', 'AGM Robot');
        $mail->addTo($mailTo, 'Checklist Manager');
        $mail->setSubject("Создан чеклист {$dateTime['store_name']} соответствия Google категорий");
        $someBinaryString = file_get_contents($checklistFile);
        $at = new Zend_Mime_Part($someBinaryString);
        $at->type        = 'text/csv';
        $at->disposition = Zend_Mime::DISPOSITION_INLINE;
        $at->encoding    = Zend_Mime::ENCODING_BASE64;
        $at->filename    = $dateTime['file_name'];
        $mail->addAttachment($at);
        try {
            $mail->send();
        } catch (Exception $ex) {
            Mage::printException($ex);
            Mage::logException($ex);
        }
    }

    /**
     * перебор результата выборки
     * и формирование корректного массива
     * для выбора значения
     * @param $rows
     * @return array
     */
    private function iterateRows($rows)
    {
        $arr = [];
        foreach ($rows as $val) {
            $arr[$val['id_gm']] = $val['chain_gm'];
        }
        return $arr;
    }

    /**
     * массив слов-синонимов
     * ключи - слова из наших категорий
     * значения - слова из Google категорий
     * @var array
     */
    protected $synonym = [
        'аксессуары' => 'принадлежности',
        'магнитолы' => 'бумбоксы',
        'телефоны DECT' => 'беспроводные телефоны',
        'смартфоны' => 'мобильные телефоны',
        'видеоняни' => 'видеокамеры',
        'радионяни' => 'радио',
        'робот-пылесосы' => 'пылесосы',
        'ручные пылесосы' => 'пылесосы',
        'чашки-непроливайки' => 'детские кружки-непроливайки',
        'фен-щетки' => 'фены',
        'мультимедийные колонки' => 'колонки',
        'пульты ду' => 'пульты дистанционного управления',
        'домашние кинотеатры' => 'системы домашних кинотеатров',
        'цитрус-прессы' => 'соковыжималки',
        'хлебопечи' => 'хлебопечки',
        'индукционные плитки' => 'электроплитки',
        'увлажнители воздуха' => 'увлажнители',
        'карманные проекторы' => 'проекторы',
        'гостиничные телевизоры' => 'телевизоры',
        'переносные колонки' => 'колонки',
        'портативное радио' => 'радио',
        'радиобудильники' => 'будильники',
        'световые будильники' => 'будильники',
        'ручные блендеры' => 'блендеры',
        'электрогрили' => 'электрические противни и грили',
        'шнековые соковыжималки' => 'соковыжималки',
        'центробежные соковыжималки' => 'соковыжималки',
        'стационарные блендеры' => 'блендеры',
        'вертикальные пылесосы' => 'пылесосы',
        'безмешковые пылесосы' => 'пылесосы',
        'мешковые пылесосы' => 'пылесосы',
        'телевизоры на базе ос android' => 'телевизоры',
    ];


    /**
     * замена слов из наших категорий
     * на слова из Google категорий
     * для облегчения поиска правильных гугл-категорий
     * @param $name
     * @return mixed
     */
    protected function runSynonym($name)
    {
        $name = mb_strtolower($name);
        $keys = array_keys($this->synonym);
        $values = array_values($this->synonym);
        $out = str_replace($keys, $values,$name);
        return $out;
    }

    /**
     * формирование массива синонимов
     * для замены наших категорий на гугловые
     * @return array
     */
    protected function getSynonymFromTable()
    {
        $arr = [];
        $resource   = Mage::getSingleton('core/resource');
        $connection = $resource->getConnection('core_read');
        $query = "SELECT * FROM $this->tableSynonym";
        $rows = $connection->fetchAll($query);
        foreach ($rows as $val) {
            $arr[$val['vpvcomm']] = $val['google'];
        }
        return $arr;
    }

    /**
     * получение синонимов из таблицы
     * @param $name
     * @param $arr
     * @return mixed
     */
    protected function runSynonymTable($name,$arr)
    {
        $name = mb_strtolower($name);
        $keys = array_keys($arr);
        $values = array_values($arr);
        $out = str_replace($keys, $values,$name);
        return $out;
    }

    /**
     * подготовка названия категории
     * для like-поиска по ней в гугл-категориях
     * @param $name
     * @return string
     */
    protected function prepareLike($name,$space=false)
    {
        $name2 = trim(preg_replace('/(.*?)([A-Za-z])/siu','$1',$name));
        if (empty($name2)) { // название категории состоит только из английских букв
            $name2 = 'EMPTY CATEGORY NAME';
        } else {
            if ($space) {
                $name2 = str_replace(' ','%',$name2);
            }
        }
        return $name2;
    }

    /**
     * вставка только уникальных значений
     * @param $query
     * @param $writeConnection
     */
    protected function insertIgnore($query,$writeConnection)
    {
        $queryMapp = "INSERT IGNORE INTO $this->tableMapping VALUES($query);";
        $writeConnection->query($queryMapp);
    }
}

$shell = new Mage_Shell_Merchant();
$shell->run();