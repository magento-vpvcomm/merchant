<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 17.01.17
 * Time: 15:19
 */

class VpvComm_GoogleMerchant_Model_Howselect
{
    /**
     * Provide available options as a value/label array
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value'=>1, 'label'=>'Выбрать все'),
            array('value'=>2, 'label'=>'Only Google Merchant ID'),
        );
    }
}