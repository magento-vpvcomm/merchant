<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 13.01.17
 * Time: 12:24
 */

class VpvComm_GoogleMerchant_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * метод возвращает номер стора
     * для POST-запроса из админки
     * для таблицы совпадений
     */
    public function getstoreidAction()
    {
        $storeId = Mage::app()->getStore()->getId();
        echo $storeId;
    }

    /**
     * Генерируем xml для Google Merchant
     */
    public function xmlAction()
    {
        $response = $this->getResponse();
        $this->loadLayout();
        $content = $this->getLayout()->getOutput();
        $response->setHeader('Content-Type', 'application/xml');
        $response->setHeader('charset', 'UTF-8', true);
        $response->appendBody(trim($content));
        $response->sendResponse();
        exit();
    }

}