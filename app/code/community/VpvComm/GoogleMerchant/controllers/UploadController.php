<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 19.01.17
 * Time: 12:23
 */

class VpvComm_GoogleMerchant_UploadController extends Mage_Adminhtml_Controller_Action
{

    public $loggedAdmin;
    public $adminRoleId;
    public $adminRoleName;
    public $adminEmail;
    public $adminUsername;
    public $adminFullName;
    public $adminSession;

    // название ключа в сессии
    private $sessCsvSuccess = 'success_csv';
    // префикс для нового имени загруженного файла
    private $prefix = 'rewr_';

    public $setCSS = "<style>body{padding: 10px 30px;}</style>";

    public function indexAction()
    {

        if ($this->checkAccess()) {

            $html = <<<HTML
<title>Checklist Upload</title>
{$this->setCSS}
<h3>Загрузка чеклиста в csv-формате</h3>
<form id="doc-form" name="doc-form" method="post" action="/adminmerchant/upload/handler" enctype="multipart/form-data">
    <input type="file" title="File" name="docname">
    <hr>
    <button type="submit" title="Save"><span>Upload</span></button>
</form>
HTML;
            echo $html;

        }

    }


    /**
     * страница удачной отправки чеклиста
     */
    public function successAction()
    {
        if ($this->checkAccess()) {
            $savedCsv = $this->adminSession->getData($this->sessCsvSuccess);
            if ($savedCsv) {
                $html = <<<HTML
{$this->setCSS}                
<p>Вы только что загрузили исправленный файл чеклиста.</p>
<p>Его название после загрузки: <strong>{$savedCsv}</strong></p>
<p><a href="/adminmerchant/upload">Вернуться к форме загрузки</a></p>
HTML;
                echo $html;
            } else {
                echo <<<HTML
<p><a href="/adminmerchant/upload">Вернуться к форме загрузки</a></p>
HTML;

            }
        }
    }


    /**
     * обработчик формы +
     * отправка извещения
     */
    public function handlerAction()
    {
        $dirPath = $admFileName = Mage::getStoreConfig('vpvcomm_googlemerchant/agm_checklist/checklist_dirpath');
        $checklistDir = empty($dirPath) ? '/media/merchant/' : $dirPath;

        if ($this->checkAccess()) {
            if(isset($_FILES['docname']['name']) && $_FILES['docname']['name'] != '') {
                try
                {
                    // путь куда будем сохранять
                    $path = Mage::getBaseDir().DS.$checklistDir.DS;
                    // название загружаемого файла
                    $fname = $_FILES['docname']['name'];
                    $fnameOrig = $fname;
                    // класс загрузчика
                    $uploader = new Varien_File_Uploader('docname');
                    // допустимые расширения файлов
                    $uploader->setAllowedExtensions(array('csv','xls'));
                    // создать директорию если не существует
                    $uploader->setAllowCreateFolders(true);
                    // если будет true, то имя загружаемого файла будет изменено
                    // если файл с таким именем уже существует
                    $uploader->setAllowRenameFiles(false);
                    $uploader->setFilesDispersion(false);
                    $fname = $this->prefix . $fname;
                    // сохранить файл по пути
                    $uploader->save($path,$fname);
                    $this->adminSession->setData($this->sessCsvSuccess, $fname);
                    $this->_redirect('adminmerchant/upload/success');

                    // -----------------------------------------------

                    $mail = new Zend_Mail('UTF-8');
                    $htmlBody = <<<HTML
На сервер загружен исправленный чеклист с названием {$fname}
HTML;
                    // определяем имя стора на основе идентификатора из названия файла
                    $storeArr = explode('-',$fnameOrig);
                    $nowStoreId = $storeArr[1];
                    $storeNameForNotice = Mage::app()->getStore($nowStoreId)->getName();

                    $mail->setBodyText($htmlBody);
                    $mail->setFrom('robot-checklist@vpvcomm.pw', 'Checklist Uploaded');
                    $mail->addTo('p.vasin@vpvcomm.pw', 'AGM Developer');
                    $mail->setSubject('upload: Загружен исправленный чеклист '.$storeNameForNotice);
                    try {
                        $mail->send();
                    } catch (Exception $ex) {
                        Mage::printException($ex);
                        Mage::logException($ex);
                    }

                }
                catch (Exception $ex)
                {
                    Mage::printException($ex);
                    Mage::logException($ex);
                }
            }
        }
    }



    /**
     * проверка прав доступа
     * и инициализация свойств класса
     * @return bool
     */
    protected function checkAccess()
    {
        $this->adminSession = Mage::getSingleton('admin/session');
        $adminuserId = $this->adminSession->getUser()->getUserId();
        $admin = Mage::getModel('admin/user')->load($adminuserId);
        $this->loggedAdmin = $this->adminSession->isLoggedIn();

        $adminRole = $admin->getRole();
        $this->adminRoleId = $adminRole->getId();
        $this->adminRoleName = $adminRole->getRoleName();

        $this->adminEmail = $admin->getEmail();
        $this->adminUsername = $admin->getUsername();
        $this->adminFullName = $admin->getLastname() . ' ' . $admin->getFirstname();

        if ($this->loggedAdmin && key_exists($this->adminRoleId, $this->goodRoles)) {
            return true;
        }
        return false;
    }

    /**
     * масссив с разрешенными ролями
     * @var array
     */
    protected $goodRoles = [
        '1' => 'Administrators',
        '44' => 'Content manager',
        '1304' => 'Маркетолог',
        '1308' => 'Менеджер проекта',
        '1309' => 'Руководитель проекта',
        '1312' => 'Контент-менеджер',
    ];

    ### ------------------------------------------

    /**
     * открыть доступ
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }

}