<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 13.01.17
 * Time: 12:24
 */

class VpvComm_GoogleMerchant_Block_Matching extends Mage_Adminhtml_Block_Abstract
    implements Varien_Data_Form_Element_Renderer_Interface
{

    private $tableTaxonomy = 'agm_merchant_taxonomy';
    private $tableMapping = 'agm_mapping_store';
    private $tableSynonym = 'agm_synonym';

    protected function getOurStoreId()
    {
        $store = Mage::app()->getRequest()->getParam('store');
        $storeId = Mage::app()->getStore($store)->getId();
        return $storeId;
    }

    public function render(Varien_Data_Form_Element_Abstract $element)
    {

        $storeId = $this->getOurStoreId();
        $storeRootCategoryId = Mage::app()->getStore($storeId)->getRootCategoryId();

        $resource   = Mage::getSingleton('core/resource');
        $connection = $resource->getConnection('core_read');

        $tr = '';

        $storeIdName = Mage::app()->getStore($storeId)->getName();

        $rootpath = Mage::getModel('catalog/category')
            ->setStoreId($storeId)
            ->load($storeRootCategoryId)
            ->getPath();

        $categories = Mage::getModel('catalog/category')->setStoreId($storeId)
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('path', array("like"=>$rootpath."/"."%"));

        ### ->addAttributeToFilter('merchant_id', NULL);

        $arr = $categories->getItems();

        // формируем массив со всеми значениями Google Merchant
        // чтобы выбирать из массива, а не запросом
        $arr_tax = $connection->fetchAll("SELECT * FROM `agm_merchant_taxonomy`");
        $arr_merch = [];
        foreach ($arr_tax as $val) {
            $arr_merch[$val['id_gm']] = $val['chain_gm'];
        }

        foreach ($arr as $item) {

            $str = '';
            if (!$item->getChildren()) {
                $categories = [];
                if(!is_null($item)) {
                    $currentCategory = $item;
                    do {
                        array_push($categories, $currentCategory->getName());
                        $currentCategory = $currentCategory->getParentCategory();
                    } while ($currentCategory->getLevel() > 1);
                }
                $categories = array_reverse($categories);
                $str = implode(' > ', $categories);
                $arrId = $item->getId();

                $merchId = $item->getData('merchant_id');

                if (key_exists($merchId,$arr_merch)) {
                    $google = $arr_merch[$merchId];
                } else {
                    $google = '';
                }

                $tr .= "<tr><td align='right'>{$str} = {$arrId}</td><td>:</td><td>{$google}</td></tr>";
            }

        }

        $table = <<<HTML
<style>
#matching_about {font-weight: normal; text-align: center !important; padding:0 0 10px 0; font-size: 16px;}
#matching {border-collapse: collapse; width: 100%;}
#matching thead tr.head {background-color: #ddd; font-weight: bold; text-transform: uppercase;}
#matching td, #matching th {border: 1px solid #000; padding: 7px;}
#matching th:nth-child(1) {text-align: right !important;}
#matching tr:nth-child(2n){background-color: #ebebeb;}
#matching tr:hover td {text-decoration: underline;font-weight: bold;}
</style>
HTML;

        $table .= "<div id=\"matching_about\"><b>Номер стора:</b> {$storeId}. <b>Номер Root Category:</b> {$storeRootCategoryId}. <b>Название Root Category:</b> {$storeIdName}.</div>";

        $table .= "<table id=\"matching\" border=\"1\" cellpadding=\"10p\" cellspacing=\"0\">    
    <thead>    
    <tr class='head'>
        <th>vpvcomm</th>
        <th>:</th>
        <th align='left'>google</th>
    </tr>
    </thead>
    <tbody>
    {$tr}
    </tbody>
</table>";

        return $table;
    }
}
