<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 13.01.17
 * Time: 12:24
 */
class VpvComm_GoogleMerchant_Block_Xml extends VpvComm_GoogleMerchant_Block_Abstract
{

    private $params = [];
    private $withId = false;
    public function __construct(array $args = [])
    {
        parent::__construct($args);
        $req = Mage::app()->getRequest();
        $source = $req->getParam('utm_source');
        $medium = $req->getParam('utm_medium');
        $content = $req->getParam('utm_content');
        $campaign = $req->getParam('utm_campaign');
        $term = $req->getParam('utm_term');
        $withId = $req->getParam('with_id');
        $params = [];
        if ($source) {
            $params['_query']['utm_source'] = $source;
        }
        if ($medium) {
            $params['_query']['utm_medium'] = $medium;
        }
        if ($content) {
            $params['_query']['utm_content'] = $content;
        }
        if ($campaign) {
            $params['_query']['utm_campaign'] = $campaign;
        }
        if ($term) {
            $params['_query']['utm_term'] = $term;
        }
        if ($withId) {
            $this->withId = true;
        }
        $this->params = $params;
    }

    /**
     * @inheritdoc
     */
    protected function _toHtml()
    {
        $collection = $this->getProductCollection();
        $xml = new SimpleXMLElement('<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0" />');
        $howSelect = Mage::getStoreConfig('vpvcomm_googlemerchant/agm_feed/feed_howselect');
        $arrMethods = [
            '1' => 'addItemsGood',
            '2' => 'addItemsMerchantOnly',
        ];
        $nameMethod = 'addItemsGood';

        if ($howSelect) {
            $nameMethod = $arrMethods[$howSelect];
        }

        $this->$nameMethod($collection, $xml);
        return $xml->asXML();
    }

    /**
     * формирование фида только для товаров
     * чьи категории имеют соответсвие в Google MErchant
     * @param $collection
     * @param SimpleXMLElement $xml
     */
    protected function addItemsMerchantOnly($collection, SimpleXMLElement &$xml)
    {
        $channel = $xml->addChild('channel');
        $channel->addChild('title',$this->getFeedTitle());
        $channel->addChild('link',$this->getFeedLink());
        $channel->addChild('description',$this->getFeedDescription());

        $showGoogleId = Mage::getStoreConfig('vpvcomm_googlemerchant/agm_feed/feed_show_google_id');

        foreach ($collection as $item) {

            $description = sprintf('%s', htmlspecialchars($item->getShortDescription())); //'<![CDATA[%s]]>'
            $category = $item->getCategory();

            $gtin = $item->getData('ean');
            $availability = $item->isInStock() ? 'in stock' : 'out of stock';
            $price = sprintf('%01.2f RUB',(float) $item->getFinalPrice());


            if ($showGoogleId) {
                $google_product_category = $this->getMerchantId($item);
            } else {
                $google_product_category = $this->getCategoryList($category);
            }

            if ($google_product_category) {
                $item->load($item->getId());
                if ($this->withId) {
                    $this->params['_query']['id'] = $item->getId();
                }
                $xmlItem = $channel->addChild('item');
                $xmlItem->addChild('xmlns:g:id', $item->getSku());
                $xmlItem->addChild('xmlns:g:title', htmlspecialchars($item->getName()));
                $xmlItem->addChild('xmlns:g:description', $description);
                $xmlItem->addChild('xmlns:g:product_type', $category ? $category->getName() : '');
                $xmlItem->addChild('xmlns:g:link', $this->getProductUrl($item));
                $xmlItem->addChild('xmlns:g:image_link', $this->getImageUrl($item));
                $xmlItem->addChild('xmlns:g:availability', $availability);
                $xmlItem->addChild('xmlns:g:price', $price);
                $xmlItem->addChild('xmlns:g:google_product_category', $google_product_category);
                $xmlItem->addChild('xmlns:g:brand', $item->getAttributeOptionValue('manufacturer'));
                $xmlItem->addChild('xmlns:g:condition', 'new');
                $xmlItem->addChild('xmlns:g:adult', 'no');
                $xmlItem->addChild('xmlns:g:gtin', $gtin);
            }
        }
    }

    protected function getProductUrl($product)
    {
        $url = $product->getUrlModel()->getUrl($product, $this->params);
        return htmlspecialchars($url);
    }


    /**
     * метод добавляющий все узлы фида
     * строго необходимые для России
     * @param $collection
     * @param SimpleXMLElement $xml
     */
    protected function addItemsGood($collection, SimpleXMLElement &$xml)
    {
        $channel = $xml->addChild('channel');
        $channel->addChild('title',$this->getFeedTitle());
        $channel->addChild('link',$this->getFeedLink());
        $channel->addChild('description',$this->getFeedDescription());

        $showGoogleId = Mage::getStoreConfig('vpvcomm_googlemerchant/agm_feed/feed_show_google_id');

        foreach ($collection as $item) {
            $item->load($item->getId());
            $description = sprintf('<![CDATA[%s]]>', htmlspecialchars($item->getShortDescription()));
            $category = $item->getCategory();
            $xmlItem = $channel->addChild('item');

            $gtin = $item->getData('ean');
            $availability = $item->isInStock() ? 'in stock' : 'out of stock';
            $price = sprintf('%01.2f RUB',(float) $item->getFinalPrice());

            $merchantId = $this->getMerchantId($item);

            if ($showGoogleId) {
                // если установлен показ гугл-айди а его нет
                // то выводим нашу цепочку
                // в одном фиде будут товары и с гугл-айди и с нашей цепочкой
                $google_product_category = empty($merchantId) ? $this->getCategoryList($category) : $merchantId;
            } else {
                $google_product_category = $this->getCategoryList($category);
            }
            if ($this->withId) {
                $this->params['_query']['id'] = $item->getId();
            }
            $xmlItem->addChild('xmlns:g:id', $item->getSku());
            $xmlItem->addChild('xmlns:g:title', htmlspecialchars($item->getName()));
            $xmlItem->addChild('xmlns:g:description', $description);
            $xmlItem->addChild('xmlns:g:product_type', $category ? $category->getName() : '');
            $xmlItem->addChild('xmlns:g:link', $this->getProductUrl($item));
            $xmlItem->addChild('xmlns:g:image_link', $this->getImageUrl($item));
            $xmlItem->addChild('xmlns:g:availability', $availability);
            $xmlItem->addChild('xmlns:g:price', $price);
            $xmlItem->addChild('xmlns:g:google_product_category', $google_product_category);
            $xmlItem->addChild('xmlns:g:brand', $item->getAttributeOptionValue('manufacturer'));
            $xmlItem->addChild('xmlns:g:condition', 'new');
            $xmlItem->addChild('xmlns:g:adult', 'no');
            $xmlItem->addChild('xmlns:g:gtin', $gtin); // $xmlItem->addChild('identifier_​exists', 'no');

        }
    }

    /**
     * @param $product
     * @return string
     */
    protected function getImageUrl($product)
    {
        return (string)Mage::helper('catalog/image')->init($product, 'image')->keepTransparency(false)->resize(800, 600);
    }


    /**
     * @param $collection
     * @param SimpleXMLElement $xml
     */
    protected function addItems($collection, SimpleXMLElement &$xml)
    {
        $channel = $xml->addChild('channel');
        /** @var VpvComm_Erp_Model_Catalog_Product $item */
        foreach ($collection as $item) {
            $item->load($item->getId());
            if ($this->withId) {
                $this->params['_query']['id'] = $item->getId();
            }
            $availability = $item->isInStock() ? 'in stock' : 'out of stock';
            $price = sprintf('%01.2f RUB',(float) $item->getFinalPrice());
            $description = sprintf('<![CDATA[%s]]>', htmlspecialchars($item->getShortDescription()));
            $category = $item->getCategory();
            $xmlItem = $channel->addChild('item');
            $xmlItem->addChild('xmlns:g:id', $item->getSku());
            $xmlItem->addChild('title', htmlspecialchars($item->getName()));
            $xmlItem->addChild('description', $description);
            $xmlItem->addChild('xmlns:g:product_type', $category ? $category->getName() : '');
            $xmlItem->addChild('link', $this->getProductUrl($item));
            $xmlItem->addChild('xmlns:g:image_link', $this->getImageUrl($item));
            $xmlItem->addChild('xmlns:g:condition', 'new');
            $xmlItem->addChild('xmlns:g:availability', $availability);
            $xmlItem->addChild('xmlns:g:price', $price);
            $xmlItem->addChild('xmlns:g:brand', $item->getAttributeOptionValue('manufacturer'));
            $xmlItem->addChild('xmlns:g:google_product_category', $this->getCategoryList($category));
        }
    }
}