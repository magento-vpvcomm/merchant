<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 13.01.17
 * Time: 12:24
 */

class VpvComm_GoogleMerchant_Block_Tips extends Mage_Adminhtml_Block_Abstract
    implements Varien_Data_Form_Element_Renderer_Interface
{
    protected $_template = 'vpvcomm/googlemerchant/tips.phtml';

    /**
     * @param Varien_Data_Form_Element_Abstract $element
     * @return mixed
     */
    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        return $this->toHtml();
    }
}
