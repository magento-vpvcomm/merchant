<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 23.01.17
 * Time: 12:17
 */

class VpvComm_GoogleMerchant_Block_Smartlink
    extends Mage_Adminhtml_Block_Abstract implements Varien_Data_Form_Element_Renderer_Interface
{
    /**
     * Render element html
     *
     * @param Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $store = Mage::app()->getRequest()->getParam('store');
        $site = Mage::app()->getStore($store)->getUrl();
        $arr = parse_url($site);
        $href = sprintf('%s://%s/%s',$arr['scheme'],$arr['host'],'googlemerchant/index/xml');
        $str = <<<HTML
<div class="comment">
<p><strong>xml-фид</strong> => <a title="Открыть фид в формате xml" href="{$href}" target="_blank">{$href}</a></p>
</div>
HTML;
        if (!$store || strpos($store,'yandex') !== false) $str = false;
        return $str;
    }
}