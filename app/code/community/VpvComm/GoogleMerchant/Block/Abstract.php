<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 13.01.17
 * Time: 12:24
 */

class VpvComm_GoogleMerchant_Block_Abstract extends Mage_Core_Block_Template
{

    /**
     * название сайта для фида
     * @return null|string
     */
    public function getFeedTitle()
    {
        $info = Mage::getStoreConfig('general/store_information');
        $str = sprintf('<![CDATA[%s]]>', htmlspecialchars($info['name']));
        return $str;
    }

    /**
     * ссылка на сайт для фида
     * @return mixed
     */
    public function getFeedLink()
    {
        return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
    }

    /**
     * описание сайта для фида
     * @return string
     */
    public function getFeedDescription()
    {
        $storePhone = Mage::getStoreConfig('general/store_information/phone');
        $storeAddress = Mage::getStoreConfig('general/store_information/address');
        $storeCountry = Mage::getStoreConfig('general/country/default');
        $str = $storeCountry.'; '.$storeAddress.'; '.$storePhone;
        $str = trim($str,';');
        $str = sprintf('<![CDATA[%s]]>', htmlspecialchars($str));
        return $str;
    }

    /**
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    protected function getProductCollection()
    {
        $price = Mage::getStoreConfig('vpvcomm_googlemerchant/agm_feed/feed_minprice');
        $price = (int)trim($price);
        $minPrice = empty($price) ? 100 : $price;
        $limit = Mage::getStoreConfig('vpvcomm_googlemerchant/agm_feed/feed_limit');
        $limit = (int)trim($limit);
        $collection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('*')
            ->addFieldToFilter('final_price', ['gteq' => $minPrice])
            ->addFieldToFilter('visibility', 4);
        $collection->getSelect()->join(['pa' => 'product_availability'],
            'e.entity_id = pa.pa_product_id AND pa_is_saleable > 0 and pa_status=0', []);
        if ($limit) {
            $collection->getSelect()->limit($limit);
        }
        return $collection;
    }

    /**
     * @param null|Mage_Catalog_Model_Category $category
     * @return string
     */
    public function getCategoryList($category)
    {
        $categories = [];
        if(!is_null($category)) {
            $currentCategory = $category;
            do {
                array_push($categories, $currentCategory->getName());
                $currentCategory = $currentCategory->getParentCategory();
            } while ($currentCategory->getLevel() > 1);
            $categories = array_reverse($categories);
        }
        $res = implode(' > ', $categories);
        return $res;
    }

    /**
     * получение Google Category Id
     * на основании VpvComm Category Id
     * @param $item
     * @return bool|int
     */
    public function getMerchantId($item)
    {
        $categoryId = $item->getCategoryId();
        $merchantId = Mage::getResourceModel('catalog/category')
            ->getAttributeRawValue($categoryId, 'merchant_id', Mage::app()->getStore()->getId());
        return $merchantId;
    }

}