-- MySQL dump 10.13  Distrib 5.7.16-10, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: pv_pfse
-- ------------------------------------------------------
-- Server version	5.7.16-10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `agm_mapping_store`
--

DROP TABLE IF EXISTS `agm_mapping_store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agm_mapping_store` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_root` int(11) NOT NULL COMMENT 'id category root',
  `id_vpvcomm` int(11) NOT NULL COMMENT 'id vpvcomm category',
  `id_google` int(11) DEFAULT NULL COMMENT 'id google category',
  PRIMARY KEY (`id`),
  UNIQUE KEY `agm_mapping_philips_google_id_philips_uindex` (`id_vpvcomm`)
) ENGINE=InnoDB AUTO_INCREMENT=733 DEFAULT CHARSET=utf8 COMMENT='соответствие id категорий Philips с Google Merchant';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agm_mapping_store`
--

LOCK TABLES `agm_mapping_store` WRITE;
/*!40000 ALTER TABLE `agm_mapping_store` DISABLE KEYS */;
INSERT INTO `agm_mapping_store` VALUES (1,496,317,404),(2,496,318,404),(3,496,321,252),(4,496,333,225),(5,496,336,244),(6,496,337,234),(7,496,339,505771),(8,496,342,267),(9,496,343,267),(10,496,351,4546),(11,496,352,4546),(12,496,355,532),(13,496,358,533),(14,496,361,4510),(15,496,378,4546),(16,496,386,5139),(17,496,389,633),(18,496,401,606),(19,496,407,1388),(20,496,412,505666),(21,496,413,750),(22,496,414,750),(23,496,415,750),(24,496,420,732),(25,496,423,7165),(26,496,429,505666),(27,496,430,505666),(28,496,435,505765),(29,496,454,2944),(30,496,460,4546),(31,496,464,565),(32,496,468,5630),(33,496,469,5296),(34,496,474,566),(35,496,478,6950),(36,496,484,155),(37,496,488,606),(445,496,319,403),(446,496,322,388),(447,496,324,396),(448,496,330,249),(449,496,331,249),(450,496,332,300),(451,496,335,273),(452,496,338,505771),(453,496,344,272),(454,496,345,265),(455,496,366,490),(456,496,367,490),(457,496,392,619),(458,496,393,619),(459,496,394,619),(460,496,395,619),(461,496,396,619),(462,496,397,5138),(463,496,400,613),(464,496,406,751),(465,496,408,5286),(466,496,421,762),(467,496,422,760),(468,496,424,747),(469,496,433,505666),(470,496,447,3006),(471,496,450,3329),(472,496,453,2690),(473,496,470,5287),(474,496,483,273),(475,496,485,501),(476,496,487,613);
/*!40000 ALTER TABLE `agm_mapping_store` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-19 15:36:23

-- --------------------------------------------------------------------------------------------

-- MySQL dump 10.13  Distrib 5.7.16-10, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: pv_pfse
-- ------------------------------------------------------
-- Server version	5.7.16-10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `agm_synonym`
--

DROP TABLE IF EXISTS `agm_synonym`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agm_synonym` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vpvcomm` varchar(128) DEFAULT NULL,
  `google` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `agm_synonym_vpvcomm_uindex` (`vpvcomm`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COMMENT='таблица синонимов между категориями vpvcomm и Google';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agm_synonym`
--

LOCK TABLES `agm_synonym` WRITE;
/*!40000 ALTER TABLE `agm_synonym` DISABLE KEYS */;
INSERT INTO `agm_synonym` VALUES (1,'аксессуары','принадлежности'),(2,'магнитолы','бумбоксы'),(3,'телефоны DECT','беспроводные телефоны'),(4,'смартфоны','мобильные телефоны'),(5,'видеоняни','видеокамеры'),(6,'радионяни','радио'),(7,'робот-пылесосы','пылесосы'),(8,'ручные пылесосы','пылесосы'),(9,'чашки-непроливайки','детские кружки-непроливайки'),(10,'фен-щетки','фены'),(11,'мультимедийные колонки','колонки'),(12,'пульты ду','пульты дистанционного управления'),(13,'домашние кинотеатры','системы домашних кинотеатров'),(14,'цитрус-прессы','соковыжималки'),(15,'хлебопечи','хлебопечки'),(16,'индукционные плитки','электроплитки'),(17,'увлажнители воздуха','увлажнители'),(18,'карманные проекторы','проекторы'),(19,'гостиничные телевизоры','телевизоры'),(20,'переносные колонки','колонки'),(21,'портативное радио','радио'),(22,'радиобудильники','будильники'),(23,'световые будильники','будильники'),(24,'ручные блендеры','блендеры'),(25,'электрогрили','электрические противни и грили'),(26,'шнековые соковыжималки','соковыжималки'),(27,'центробежные соковыжималки','соковыжималки'),(28,'стационарные блендеры','блендеры'),(29,'вертикальные пылесосы','пылесосы'),(30,'безмешковые пылесосы','пылесосы'),(31,'мешковые пылесосы','пылесосы'),(32,'телевизоры на базе ос android','телевизоры');
/*!40000 ALTER TABLE `agm_synonym` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-19 15:38:03
