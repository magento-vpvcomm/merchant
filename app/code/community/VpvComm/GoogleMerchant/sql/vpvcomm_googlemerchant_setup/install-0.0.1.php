<?php

$this->startSetup();

$attribute  = array(
    'group' => "Yandex Market",
    'label'=> 'Merchant ID',
    'note' => 'Номер категории из Google Merchant',
    'input' => 'text',
    'type' => 'int',
    'visible' => true,
    'required' => false,
    'user_defined' => true,
    'default' => "",
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL
);
$this->addAttribute('catalog_category', 'merchant_id', $attribute);

$this->endSetup();
